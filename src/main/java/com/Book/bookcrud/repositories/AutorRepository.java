package com.Book.bookcrud.repositories;

import com.Book.bookcrud.entities.Autor;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface AutorRepository extends CrudRepository<Autor,Integer>
{
}
