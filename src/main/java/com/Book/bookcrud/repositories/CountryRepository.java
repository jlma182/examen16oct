package com.Book.bookcrud.repositories;

import com.Book.bookcrud.entities.Country;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CountryRepository extends CrudRepository <Country, Integer>
{
}
