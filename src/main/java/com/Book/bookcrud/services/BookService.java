package com.Book.bookcrud.services;

import com.Book.bookcrud.entities.Book;

public interface BookService {

    Iterable<Book> listAllBooks();
    Book findBookById(Integer id);
    void createBook(Book book);
    void deleteBook(Integer id);
    void saveBook(Book vbook);
}