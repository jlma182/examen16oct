package com.Book.bookcrud.services;

import com.Book.bookcrud.entities.Country;

public interface CountryService
{
    Iterable<Country> listAllCountry();

    void saveCountry(Country country);

    Country getCountry(Integer id);
}