package com.Book.bookcrud.services;

import com.Book.bookcrud.entities.Book;
import com.Book.bookcrud.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService
{

    BookRepository bookRepository;

    @Autowired
    @Qualifier(value = "bookRepository")
    public void setBookRepository(BookRepository bookRepository)
    {
        this.bookRepository = bookRepository;
    }

    @Override
    public Iterable<Book> listAllBooks()
    {
        Iterable<Book> libros= bookRepository.findAll();
        return libros;
    }

    @Override
    public Book findBookById(Integer id)
    {
        Optional<Book> libro = bookRepository.findById(id);
        return libro.get();
    }

    @Override
    public void createBook(Book book)
    {
        bookRepository.save(book);

    }

    @Override
    public void deleteBook(Integer id)
    {
        bookRepository.deleteById(id);
    }

    @Override
    public void saveBook(Book vbook)
    {
        bookRepository.save(vbook);
    }

}
