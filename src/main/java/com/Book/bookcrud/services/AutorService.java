package com.Book.bookcrud.services;

import com.Book.bookcrud.entities.Autor;

public interface AutorService
{
    Iterable<Autor> listAllAutores();
    Autor findAutorById(Integer id);
    void createAutor (Autor autor);
    void deleteAutor(Integer id);
    void saveAutor(Autor vautor);
}
